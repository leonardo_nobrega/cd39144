
;; cqlsh -e "drop keyspace demo_analytics; drop keyspace demo_analytics_high_rep;"

;; load namespaces
(require '[cenx.plataea.cassandra.init :as i]
         '[cenx.plataea.cassandra.long-term.aggregate :as agg]
         '[cenx.plataea.cassandra.long-term.aggregate-test :as t]
         '[cenx.plataea.cassandra.long-term.base :as base]
         '[cenx.plataea.cassandra.vnf :as vnf]
         '[cenx.plataea.spark.test :as st])

(defmacro demo-context
  [sess sc & expressions]
  `(with-redefs [cenx.plataea.cassandra/keyspace-prefix "demo_"
                 ;; set aggregation interval to one minute
                 cenx.plataea.cassandra.long-term.aggregate/time-interval-10us
                 (* 60 1000 100)]
     (cenx.plataea.spark.test/with-cassandra-session-and-spark-context
       ~sess
       ~sc
       ~@expressions)))

;; create and populate short-term
(demo-context session sc
              (i/init-keyspaces session)
              ;; create vnf_data
              (vnf/create-table session)
              ;; insert test data
              (t/populate-short-term-table sc "vnf_data" t/test-data))

;; cqlsh -e "select * from demo_analytics.vnf_data;"

;; query with delorean

;; (go true)
;; http://127.0.0.1:8081/graph/time-series/10.75.14.222?metric-name=vnf-kpi&start-time=1483620720000&end-time=1483620850000&metric-field=auto_hrStorageUsed
;; switch to ltq/query
;; (load-file "/Users/leonardo.nobrega/Documents/src/delorean/src/cenx/delorean/resources/time_series.clj")

;; create long-term
(demo-context session sc
              (base/create-table session))

;; cqlsh -e "describe table demo_analytics.long_term;"

;; populate long-term
(demo-context session sc
              ;; create processing_states
              (i/create-proc-states-table session)
              ;; read from short-term into long-term
              (agg/populate-long-term-table sc session))

;; cqlsh -e "select * from demo_analytics.long_term;"

;; compare sizes

;; nodetool flush -- demo_analytics vnf_data long_term;
;; nodetool compact -- demo_analytics vnf_data long_term;
;; cd /private/var/lib/cassandra/data/demo_analytics
;; ls -l vnf_data
;; ls -l long_term
;; /usr/local/dse-4.6.7/resources/cassandra/bin/sstable2json vnf_data/demo_analytics-vnf_data-jb-2-Data.db
;; /usr/local/dse-4.6.7/resources/cassandra/bin/sstable2json long_term/demo_analytics-long_term-jb-2-Data.db

;; clear vnf_data

;; cqlsh -e "truncate demo_analytics.vnf_data;"
;; cqlsh -e "select * from demo_analytics.vnf_data;"
;; cqlsh -e "select * from demo_analytics.long_term;"
