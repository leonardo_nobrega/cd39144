(ns cd39144.core)

(defn time-value-pairs
  [test-data start len metric]
  (let [time-interval (* 60 1000 100)
        metric-index 1
        value-index 2
        timestamp-index 3]
    (as-> test-data $
         (nthrest $ start)
         (take len $)
         (filter #(= (get % metric-index) metric) $)
         (map (juxt #(rem (get % timestamp-index)
                          time-interval)
                    #(get % value-index))
              $)
         (vec $))))
