[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building plataea 1.8.28.14-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-dependency-plugin:2.8:tree (default-cli) @ plataea ---
[INFO] cenx:plataea:jar:1.8.28.14-SNAPSHOT
[INFO] +- org.clojure:clojure:jar:1.7.0:compile
[INFO] +- cenx:prometheus:jar:0.7.12:compile
[INFO] |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  +- log4j:log4j:jar:1.2.17:compile
[INFO] |  +- com.climate:serializable-fn:jar:0.0.3:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.3.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for conflict with 2.10.5)
[INFO] |  \- inflections:inflections:jar:0.9.14:compile
[INFO] |     \- noencore:noencore:jar:0.1.20:compile
[INFO] |        \- (commons-codec:commons-codec:jar:1.10:compile - omitted for conflict with 1.7)
[INFO] +- cenx:ramesseum:jar:2.4.63.15:compile
[INFO] |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  +- (org.clojure:data.csv:jar:0.1.3:compile - omitted for duplicate)
[INFO] |  +- iota:iota:jar:1.1.3:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- org.codehaus.jsr166-mirror:jsr166y:jar:1.7.0:compile
[INFO] |  +- org.clojure:data.xml:jar:0.0.8:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (org.clojure:test.check:jar:0.9.0:compile - omitted for duplicate)
[INFO] |  +- (cenx:alexandria:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  +- cenx:odysseus:jar:0.1.29:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  |  +- (cc.qbits:alia:jar:2.5.2:compile - omitted for duplicate)
[INFO] |  |  +- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.6:compile - omitted for duplicate)
[INFO] |  |  +- (com.stuartsierra:component:jar:0.3.1:compile - omitted for conflict with 0.2.3)
[INFO] |  |  +- (com.palletops:leaven:jar:0.3.1:compile - omitted for conflict with 0.3.0)
[INFO] |  |  +- (com.taoensso:timbre:jar:4.2.1:compile - omitted for conflict with 4.0.2)
[INFO] |  |  \- (cenx:plutus:jar:0.1.6:compile - omitted for conflict with 0.1.3)
[INFO] |  +- (cenx:pack:jar:0.1.5:compile - omitted for duplicate)
[INFO] |  +- (cenx:proteus:jar:0.3.0:compile - omitted for duplicate)
[INFO] |  +- cenx:thoth:jar:0.7.5:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- (cenx:proteus:jar:0.1.6:compile - omitted for conflict with 0.3.0)
[INFO] |  |  +- com.novemberain:pantomime:jar:2.1.0:compile
[INFO] |  |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  |  \- org.apache.tika:tika-core:jar:1.5:compile
[INFO] |  |  +- org.apache.poi:poi:jar:3.9:compile
[INFO] |  |  |  \- (commons-codec:commons-codec:jar:1.5:compile - omitted for conflict with 1.10)
[INFO] |  |  +- org.apache.poi:poi-ooxml:jar:3.9:compile
[INFO] |  |  |  +- (org.apache.poi:poi:jar:3.9:compile - omitted for duplicate)
[INFO] |  |  |  +- org.apache.poi:poi-ooxml-schemas:jar:3.9:compile
[INFO] |  |  |  |  \- org.apache.xmlbeans:xmlbeans:jar:2.3.0:compile
[INFO] |  |  |  |     \- (stax:stax-api:jar:1.0.1:compile - omitted for duplicate)
[INFO] |  |  |  \- dom4j:dom4j:jar:1.6.1:compile
[INFO] |  |  |     \- xml-apis:xml-apis:jar:1.0.b2:compile
[INFO] |  |  +- com.github.kyleburton:clj-xpath:jar:1.4.3:compile
[INFO] |  |  |  \- xalan:xalan:jar:2.7.1:compile
[INFO] |  |  |     \- xalan:serializer:jar:2.7.1:compile
[INFO] |  |  |        \- (xml-apis:xml-apis:jar:1.3.04:compile - omitted for conflict with 1.0.b2)
[INFO] |  |  +- clojure-csv:clojure-csv:jar:2.0.1:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.3.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  +- (clj-time:clj-time:jar:0.11.0:compile - omitted for duplicate)
[INFO] |  +- (com.taoensso:timbre:jar:4.1.1:compile - omitted for duplicate)
[INFO] |  +- (inflections:inflections:jar:0.9.13:compile - omitted for conflict with 0.9.14)
[INFO] |  \- riveted:riveted:jar:0.0.9:compile
[INFO] |     \- com.ximpleware:vtd-xml:jar:2.11:compile
[INFO] +- cenx:proteus:jar:0.3.0:compile
[INFO] |  +- com.keminglabs:cljx:jar:0.4.0:compile
[INFO] |  |  +- org.clojure:core.match:jar:0.2.0:compile
[INFO] |  |  +- org.clojars.trptcolin:sjacket:jar:0.1.0.6:compile
[INFO] |  |  |  +- net.cgrand:regex:jar:1.1.0:compile
[INFO] |  |  |  \- net.cgrand:parsley:jar:0.9.1:compile
[INFO] |  |  |     \- (net.cgrand:regex:jar:1.1.0:compile - omitted for duplicate)
[INFO] |  |  +- com.cemerick:piggieback:jar:0.1.3:compile
[INFO] |  |  |  +- org.clojure:tools.nrepl:jar:0.2.3:compile
[INFO] |  |  |  \- (org.clojure:clojurescript:jar:0.0-2080:compile - omitted for conflict with 1.7.122)
[INFO] |  |  \- watchtower:watchtower:jar:0.1.1:compile
[INFO] |  \- (clj-time:clj-time:jar:0.6.0:compile - omitted for conflict with 0.11.0)
[INFO] +- cenx:stentor:jar:1.5.18:compile
[INFO] |  +- com.palletops:leaven:jar:0.3.0:compile
[INFO] |  |  +- com.palletops:api-builder:jar:0.3.0:compile
[INFO] |  |  |  +- org.clojure:tools.macro:jar:0.1.5:compile
[INFO] |  |  |  \- (prismatic:schema:jar:0.2.6:compile - omitted for conflict with 0.4.2)
[INFO] |  |  \- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  +- (com.taoensso:timbre:jar:3.3.1:compile - omitted for conflict with 4.1.1)
[INFO] |  +- (cenx:plutus:jar:0.1.3:compile - omitted for conflict with 0.1.6)
[INFO] |  +- (cenx:alexandria:jar:1.0.2:compile - omitted for conflict with 2.2.0)
[INFO] |  +- cenx:cerberus:jar:0.2.1:compile
[INFO] |  |  +- (org.clojure:core.async:jar:0.1.267.0-0d7780-alpha:compile - omitted for conflict with 0.1.346.0-17112a-alpha)
[INFO] |  |  +- (cenx:alexandria:jar:1.0.0:compile - omitted for conflict with 2.2.0)
[INFO] |  |  +- cenx:lifecycle:jar:0.1.1:compile
[INFO] |  |  +- (clj-time:clj-time:jar:0.7.0:compile - omitted for conflict with 0.11.0)
[INFO] |  |  +- (http-kit:http-kit:jar:2.1.18:compile - omitted for conflict with 2.1.19)
[INFO] |  |  +- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  |  +- info.hoetzel:clj-nio2:jar:0.1.1:compile
[INFO] |  |  \- (com.taoensso:timbre:jar:3.2.1:compile - omitted for conflict with 4.1.1)
[INFO] |  +- (cenx:hades:jar:0.3.4:compile - omitted for duplicate)
[INFO] |  +- zookeeper-clj:zookeeper-clj:jar:0.9.3:compile
[INFO] |  |  +- org.apache.zookeeper:zookeeper:jar:3.4.0:compile
[INFO] |  |  |  +- (org.slf4j:slf4j-api:jar:1.6.1:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- (log4j:log4j:jar:1.2.15:compile - omitted for conflict with 1.2.17)
[INFO] |  |  |  \- (jline:jline:jar:0.9.94:compile - omitted for conflict with 1.0)
[INFO] |  |  +- (log4j:log4j:jar:1.2.17:compile - omitted for duplicate)
[INFO] |  |  \- (commons-codec:commons-codec:jar:1.7:compile - omitted for conflict with 1.5)
[INFO] |  +- im.chit:ribol:jar:0.4.0:compile
[INFO] |  \- prismatic:schema:jar:1.0.1:compile
[INFO] +- cenx:plutus:jar:0.1.6:compile
[INFO] |  +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  \- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] +- cenx:alexandria:jar:2.2.0:compile
[INFO] |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  +- org.clojure:clojurescript:jar:1.7.122:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  |  +- com.google.javascript:closure-compiler:jar:v20150729:compile
[INFO] |  |  |  +- com.google.javascript:closure-compiler-externs:jar:v20150729:compile
[INFO] |  |  |  +- args4j:args4j:jar:2.0.26:compile
[INFO] |  |  |  +- (com.google.guava:guava:jar:18.0:compile - omitted for conflict with 14.0.1)
[INFO] |  |  |  +- com.google.protobuf:protobuf-java:jar:2.5.0:compile
[INFO] |  |  |  +- com.google.code.gson:gson:jar:2.2.4:compile
[INFO] |  |  |  \- (com.google.code.findbugs:jsr305:jar:1.3.9:compile - omitted for duplicate)
[INFO] |  |  +- org.clojure:google-closure-library:jar:0.0-20150805-acd8b553:compile
[INFO] |  |  |  \- org.clojure:google-closure-library-third-party:jar:0.0-20150805-acd8b553:compile
[INFO] |  |  +- (org.clojure:data.json:jar:0.2.6:compile - omitted for conflict with 0.2.2)
[INFO] |  |  +- org.mozilla:rhino:jar:1.7R5:compile
[INFO] |  |  \- (org.clojure:tools.reader:jar:0.10.0-alpha3:compile - omitted for conflict with 0.9.2)
[INFO] |  +- org.clojure:core.memoize:jar:0.5.8:compile
[INFO] |  |  +- (org.clojure:core.cache:jar:0.6.4:compile - omitted for duplicate)
[INFO] |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  +- (prismatic:schema:jar:0.4.2:compile - omitted for conflict with 1.0.1)
[INFO] |  +- (com.taoensso:timbre:jar:4.0.2:compile - omitted for conflict with 4.1.1)
[INFO] |  +- clojail:clojail:jar:1.0.6:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- bultitude:bultitude:jar:0.1.6:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.2.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- serializable-fn:serializable-fn:jar:1.1.3:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.3.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- org.flatland:useful:jar:0.9.3:compile
[INFO] |  |     +- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |     \- (org.clojure:tools.macro:jar:0.1.1:compile - omitted for conflict with 0.1.5)
[INFO] |  +- org.slf4j:slf4j-api:jar:1.7.12:compile
[INFO] |  \- metrics-clojure:metrics-clojure:jar:2.5.1:compile
[INFO] |     +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |     \- io.dropwizard.metrics:metrics-core:jar:3.1.1:compile
[INFO] |        \- (org.slf4j:slf4j-api:jar:1.7.7:compile - omitted for conflict with 1.7.12)
[INFO] +- cenx:anamnesis:jar:1.4.3:compile
[INFO] |  +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (com.palletops:leaven:jar:0.3.1:compile - omitted for conflict with 0.3.0)
[INFO] |  +- cenx:corona:jar:2.4.3:compile
[INFO] |  |  +- (org.clojure:clojurescript:jar:0.0-3211:compile - omitted for conflict with 1.7.122)
[INFO] |  |  +- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  |  +- (cenx:experior:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  |  +- (cenx:flight:jar:0.3.5:compile - omitted for conflict with 1.4.6)
[INFO] |  |  +- (cenx:proteus:jar:0.3.0:compile - omitted for duplicate)
[INFO] |  |  +- (cenx:janus:jar:0.4.4:compile - omitted for conflict with 0.21.0)
[INFO] |  |  +- (prismatic:schema:jar:0.4.2:compile - omitted for conflict with 1.0.1)
[INFO] |  |  +- (cheshire:cheshire:jar:5.4.0:compile - omitted for duplicate)
[INFO] |  |  +- org.apache.solr:solr-core:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-analyzers-common:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-analyzers-kuromoji:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-analyzers-phonetic:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-codecs:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-core:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-expressions:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-grouping:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-highlighter:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-join:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-memory:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-misc:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-queries:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-queryparser:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-spatial:jar:4.10.2:compile
[INFO] |  |  |  +- org.apache.lucene:lucene-suggest:jar:4.10.2:compile
[INFO] |  |  |  +- (org.apache.solr:solr-solrj:jar:4.10.2:compile - omitted for duplicate)
[INFO] |  |  |  +- com.carrotsearch:hppc:jar:0.5.2:compile
[INFO] |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  +- (com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:jar:1.2:compile - omitted for conflict with 1.3)
[INFO] |  |  |  +- com.spatial4j:spatial4j:jar:0.4.1:compile
[INFO] |  |  |  +- (commons-cli:commons-cli:jar:1.2:compile - omitted for conflict with 1.1)
[INFO] |  |  |  +- (commons-codec:commons-codec:jar:1.9:compile - omitted for conflict with 1.7)
[INFO] |  |  |  +- commons-configuration:commons-configuration:jar:1.6:compile
[INFO] |  |  |  +- commons-io:commons-io:jar:2.3:compile
[INFO] |  |  |  +- commons-lang:commons-lang:jar:2.6:compile
[INFO] |  |  |  +- (dom4j:dom4j:jar:1.6.1:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.antlr:antlr-runtime:jar:3.5:compile - omitted for conflict with 3.5.2)
[INFO] |  |  |  +- org.apache.hadoop:hadoop-annotations:jar:2.2.0:compile
[INFO] |  |  |  +- org.apache.hadoop:hadoop-auth:jar:2.2.0:compile
[INFO] |  |  |  +- org.apache.hadoop:hadoop-common:jar:2.2.0:compile
[INFO] |  |  |  |  \- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  +- org.apache.hadoop:hadoop-hdfs:jar:2.2.0:compile
[INFO] |  |  |  +- (org.apache.httpcomponents:httpclient:jar:4.3.1:compile - omitted for conflict with 4.2)
[INFO] |  |  |  +- (org.apache.httpcomponents:httpcore:jar:4.3:compile - omitted for conflict with 4.2)
[INFO] |  |  |  +- org.apache.httpcomponents:httpmime:jar:4.3.1:compile
[INFO] |  |  |  +- (org.apache.zookeeper:zookeeper:jar:3.4.6:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  +- org.codehaus.woodstox:wstx-asl:jar:3.2.7:compile
[INFO] |  |  |  +- org.eclipse.jetty:jetty-continuation:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- org.eclipse.jetty:jetty-deploy:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- org.eclipse.jetty:jetty-http:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- org.eclipse.jetty:jetty-io:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- org.eclipse.jetty:jetty-jmx:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- (org.eclipse.jetty:jetty-security:jar:8.1.10.v20130312:compile - omitted for conflict with 8.1.14.v20131031)
[INFO] |  |  |  +- (org.eclipse.jetty:jetty-server:jar:8.1.10.v20130312:compile - omitted for conflict with 8.1.14.v20131031)
[INFO] |  |  |  +- org.eclipse.jetty:jetty-servlet:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- (org.eclipse.jetty:jetty-util:jar:8.1.10.v20130312:compile - omitted for conflict with 8.1.14.v20131031)
[INFO] |  |  |  +- org.eclipse.jetty:jetty-webapp:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- org.eclipse.jetty:jetty-xml:jar:8.1.10.v20130312:compile
[INFO] |  |  |  +- org.eclipse.jetty.orbit:javax.servlet:jar:3.0.0.v201112011016:compile
[INFO] |  |  |  +- org.noggit:noggit:jar:0.5:compile
[INFO] |  |  |  +- org.ow2.asm:asm:jar:4.1:compile
[INFO] |  |  |  +- org.ow2.asm:asm-commons:jar:4.1:compile
[INFO] |  |  |  +- org.restlet.jee:org.restlet:jar:2.1.1:compile
[INFO] |  |  |  +- org.restlet.jee:org.restlet.ext.servlet:jar:2.1.1:compile
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.6:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- org.apache.solr:solr-solrj:jar:4.10.2:compile
[INFO] |  |  |  +- (commons-io:commons-io:jar:2.3:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.httpcomponents:httpclient:jar:4.3.1:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.httpcomponents:httpcore:jar:4.3:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.httpcomponents:httpmime:jar:4.3.1:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.zookeeper:zookeeper:jar:3.4.6:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  +- (org.codehaus.woodstox:wstx-asl:jar:3.2.7:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.noggit:noggit:jar:0.5:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.6:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (com.cemerick:url:jar:0.1.1:compile - omitted for duplicate)
[INFO] |  |  +- (com.palletops:leaven:jar:0.3.1:compile - omitted for conflict with 0.3.0)
[INFO] |  |  +- (com.taoensso:timbre:jar:4.0.2:compile - omitted for conflict with 4.1.1)
[INFO] |  |  +- hiccup:hiccup:jar:1.0.5:compile
[INFO] |  |  +- clucy:clucy:jar:0.4.0:compile
[INFO] |  |  |  +- (org.apache.lucene:lucene-core:jar:4.2.0:compile - omitted for conflict with 4.10.2)
[INFO] |  |  |  +- (org.apache.lucene:lucene-queryparser:jar:4.2.0:compile - omitted for conflict with 4.10.2)
[INFO] |  |  |  +- (org.apache.lucene:lucene-analyzers-common:jar:4.2.0:compile - omitted for conflict with 4.10.2)
[INFO] |  |  |  \- (org.apache.lucene:lucene-highlighter:jar:4.2.0:compile - omitted for conflict with 4.10.2)
[INFO] |  |  \- (com.cemerick:austin:jar:0.1.6:compile - omitted for duplicate)
[INFO] |  +- (cenx:pack:jar:0.1.3:compile - omitted for conflict with 0.1.5)
[INFO] |  +- cenx:ninurta:jar:0.3.6:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- (org.clojure:core.async:jar:0.1.338.0-5c5012-alpha:compile - omitted for conflict with 0.1.346.0-17112a-alpha)
[INFO] |  |  +- (clj-kafka:clj-kafka:jar:0.2.8-0.8.1.1:compile - omitted for conflict with 0.2.1-0.8)
[INFO] |  |  +- org.apache.kafka:kafka-clients:jar:0.8.2.1:compile
[INFO] |  |  |  +- (org.slf4j:slf4j-api:jar:1.7.6:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- (net.jpountz.lz4:lz4:jar:1.2.0:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.xerial.snappy:snappy-java:jar:1.1.1.6:compile - omitted for duplicate)
[INFO] |  |  +- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  |  +- (com.palletops:leaven:jar:0.3.0:compile - omitted for duplicate)
[INFO] |  |  +- (potemkin:potemkin:jar:0.3.12:compile - omitted for duplicate)
[INFO] |  |  \- (com.taoensso:timbre:jar:3.4.0:compile - omitted for conflict with 4.1.1)
[INFO] |  +- (cenx:alexandria:jar:1.0.9:compile - omitted for conflict with 2.2.0)
[INFO] |  +- (clj-time:clj-time:jar:0.9.0:compile - omitted for conflict with 0.11.0)
[INFO] |  \- commons-logging:commons-logging:jar:1.1.3:compile
[INFO] +- cenx:flight:jar:1.4.6:compile
[INFO] |  +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (org.clojure:clojurescript:jar:0.0-3211:compile - omitted for conflict with 1.7.122)
[INFO] |  +- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  +- (cenx:alexandria:jar:1.0.7:compile - omitted for conflict with 2.2.0)
[INFO] |  +- (cenx:cerberus:jar:0.2.2:compile - omitted for conflict with 0.2.1)
[INFO] |  +- (cenx:pack:jar:0.1.5:compile - omitted for duplicate)
[INFO] |  +- com.cemerick:url:jar:0.1.1:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- pathetic:pathetic:jar:0.5.0:compile
[INFO] |  |     \- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (com.palletops:leaven:jar:0.3.1:compile - omitted for conflict with 0.3.0)
[INFO] |  +- com.taoensso:sente:jar:1.5.0:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  |  +- (org.clojure:tools.reader:jar:0.9.2:compile - omitted for conflict with 0.10.0-alpha3)
[INFO] |  |  +- (com.taoensso:encore:jar:1.37.0:compile - omitted for conflict with 2.4.2)
[INFO] |  |  \- (com.taoensso:timbre:jar:3.4.0:compile - omitted for conflict with 4.1.1)
[INFO] |  +- (com.taoensso:timbre:jar:4.0.2:compile - omitted for conflict with 4.1.1)
[INFO] |  +- com.cemerick:clojurescript.test:jar:0.3.3:compile
[INFO] |  |  \- (org.clojure:clojurescript:jar:0.0-2411:compile - omitted for conflict with 1.7.122)
[INFO] |  +- (clj-time:clj-time:jar:0.8.0:compile - omitted for conflict with 0.11.0)
[INFO] |  +- commons-net:commons-net:jar:3.3:compile
[INFO] |  +- http-kit:http-kit:jar:2.1.19:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  +- (prismatic:schema:jar:0.4.2:compile - omitted for conflict with 1.0.1)
[INFO] |  \- com.cemerick:austin:jar:0.1.6:compile
[INFO] |     +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |     +- (org.clojure:clojurescript:jar:0.0-2665:compile - omitted for conflict with 1.7.122)
[INFO] |     \- (com.cemerick:piggieback:jar:0.1.4:compile - omitted for conflict with 0.1.3)
[INFO] +- cenx:pack:jar:0.1.5:compile
[INFO] |  +- (com.cemerick:austin:jar:0.1.6:compile - omitted for duplicate)
[INFO] |  +- (com.keminglabs:cljx:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  +- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  +- (org.clojure:clojurescript:jar:0.0-3058:compile - omitted for conflict with 1.7.122)
[INFO] |  +- (org.clojure:data.xml:jar:0.0.8:compile - omitted for duplicate)
[INFO] |  +- cheshire:cheshire:jar:5.4.0:compile
[INFO] |  |  +- com.fasterxml.jackson.core:jackson-core:jar:2.4.4:compile
[INFO] |  |  +- com.fasterxml.jackson.dataformat:jackson-dataformat-smile:jar:2.4.4:compile
[INFO] |  |  |  \- (com.fasterxml.jackson.core:jackson-core:jar:2.4.4:compile - omitted for duplicate)
[INFO] |  |  +- com.fasterxml.jackson.dataformat:jackson-dataformat-cbor:jar:2.4.4:compile
[INFO] |  |  |  \- (com.fasterxml.jackson.core:jackson-core:jar:2.4.4:compile - omitted for duplicate)
[INFO] |  |  \- tigris:tigris:jar:0.1.1:compile
[INFO] |  +- com.damballa:abracad:jar:0.4.11:compile
[INFO] |  |  \- org.apache.avro:avro:jar:1.7.5:compile
[INFO] |  |     +- com.thoughtworks.paranamer:paranamer:jar:2.3:compile
[INFO] |  |     +- (org.xerial.snappy:snappy-java:jar:1.0.5:compile - omitted for conflict with 1.1.1.6)
[INFO] |  |     +- org.apache.commons:commons-compress:jar:1.4.1:compile
[INFO] |  |     |  \- org.tukaani:xz:jar:1.0:compile
[INFO] |  |     \- (org.slf4j:slf4j-api:jar:1.6.4:compile - omitted for conflict with 1.7.12)
[INFO] |  +- (cenx:alexandria:jar:1.0.6:compile - omitted for conflict with 2.2.0)
[INFO] |  +- (com.cemerick:url:jar:0.1.1:compile - omitted for duplicate)
[INFO] |  +- com.cognitect:transit-clj:jar:0.8.269:compile
[INFO] |  |  \- com.cognitect:transit-java:jar:0.8.276:compile
[INFO] |  |     +- com.fasterxml.jackson.datatype:jackson-datatype-json-org:jar:2.3.2:compile
[INFO] |  |     |  +- (com.fasterxml.jackson.core:jackson-core:jar:2.3.2:compile - omitted for conflict with 2.4.4)
[INFO] |  |     |  +- (com.fasterxml.jackson.core:jackson-databind:jar:2.3.2:compile - omitted for conflict with 2.3.1)
[INFO] |  |     |  \- (org.json:json:jar:20090211:compile - omitted for duplicate)
[INFO] |  |     +- org.msgpack:msgpack:jar:0.6.10:compile
[INFO] |  |     |  +- (com.googlecode.json-simple:json-simple:jar:1.1.1:compile - omitted for conflict with 1.1)
[INFO] |  |     |  \- org.javassist:javassist:jar:3.18.1-GA:compile
[INFO] |  |     \- org.apache.directory.studio:org.apache.commons.codec:jar:1.8:compile
[INFO] |  |        \- (commons-codec:commons-codec:jar:1.8:compile - omitted for conflict with 1.7)
[INFO] |  \- com.cognitect:transit-cljs:jar:0.8.205:compile
[INFO] |     \- com.cognitect:transit-js:jar:0.8.755:compile
[INFO] +- cenx:baldr:jar:1.3.1:compile
[INFO] |  +- (cenx:athena:jar:1.2.51:compile - omitted for duplicate)
[INFO] |  +- cenx:mercury:jar:1.8.3:compile
[INFO] |  |  +- (org.clojure:clojurescript:jar:0.0-3211:compile - omitted for conflict with 1.7.122)
[INFO] |  |  +- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] |  |  +- (org.clojure:core.cache:jar:0.6.4:compile - omitted for duplicate)
[INFO] |  |  +- (org.clojure:core.memoize:jar:0.5.6:compile - omitted for conflict with 0.5.8)
[INFO] |  |  +- org.clojure:data.zip:jar:0.1.1:compile
[INFO] |  |  +- (org.clojure:data.xml:jar:0.0.8:compile - omitted for duplicate)
[INFO] |  |  +- (cenx:flight:jar:1.4.4:compile - omitted for conflict with 1.4.6)
[INFO] |  |  +- (cenx:alexandria:jar:2.1.1:compile - omitted for conflict with 2.2.0)
[INFO] |  |  +- (cenx:pack:jar:0.1.5:compile - omitted for duplicate)
[INFO] |  |  +- (cenx:plutus:jar:0.1.6:compile - omitted for duplicate)
[INFO] |  |  +- com.cemerick:friend:jar:0.2.1:compile
[INFO] |  |  |  +- (ring:ring-core:jar:1.2.0:compile - omitted for conflict with 1.4.0)
[INFO] |  |  |  +- slingshot:slingshot:jar:0.12.1:compile
[INFO] |  |  |  +- robert:hooke:jar:1.1.2:compile
[INFO] |  |  |  +- org.clojure:core.incubator:jar:0.1.1:compile
[INFO] |  |  |  +- (org.mindrot:jbcrypt:jar:0.3m:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.clojure:core.cache:jar:0.6.2:compile - omitted for conflict with 0.6.4)
[INFO] |  |  |  +- org.openid4java:openid4java-nodeps:jar:0.9.6:compile
[INFO] |  |  |  |  +- (commons-logging:commons-logging:jar:1.1.1:compile - omitted for conflict with 1.1.3)
[INFO] |  |  |  |  \- net.jcip:jcip-annotations:jar:1.0:compile
[INFO] |  |  |  +- com.google.inject:guice:jar:2.0:compile
[INFO] |  |  |  |  \- aopalliance:aopalliance:jar:1.0:compile
[INFO] |  |  |  +- net.sourceforge.nekohtml:nekohtml:jar:1.9.10:compile
[INFO] |  |  |  |  \- xerces:xercesImpl:jar:2.8.1:compile
[INFO] |  |  |  \- (org.apache.httpcomponents:httpclient:jar:4.2.1:compile - omitted for conflict with 4.3.1)
[INFO] |  |  +- (com.cemerick:url:jar:0.1.1:compile - omitted for duplicate)
[INFO] |  |  +- org.immutant:immutant:jar:2.0.2:compile
[INFO] |  |  |  +- org.immutant:caching:jar:2.0.2:compile
[INFO] |  |  |  |  +- (org.immutant:core:jar:2.0.2:compile - omitted for duplicate)
[INFO] |  |  |  |  +- org.projectodd.wunderboss:wunderboss-caching:jar:0.8.1:compile
[INFO] |  |  |  |  |  +- (org.projectodd.wunderboss:wunderboss-core:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  |  |  \- org.infinispan:infinispan-core:jar:6.0.2.Final:compile
[INFO] |  |  |  |  |     +- org.infinispan:infinispan-commons:jar:6.0.2.Final:compile
[INFO] |  |  |  |  |     |  \- (org.jboss.logging:jboss-logging:jar:3.1.2.GA:compile - omitted for conflict with 3.1.0.GA)
[INFO] |  |  |  |  |     +- (org.jgroups:jgroups:jar:3.4.1.Final:compile - omitted for conflict with 3.2.12.Final)
[INFO] |  |  |  |  |     +- org.jboss.spec.javax.transaction:jboss-transaction-api_1.1_spec:jar:1.0.1.Final:compile
[INFO] |  |  |  |  |     +- org.jboss.marshalling:jboss-marshalling-river:jar:1.4.4.Final:compile
[INFO] |  |  |  |  |     |  \- (org.jboss.marshalling:jboss-marshalling:jar:1.4.4.Final:compile - omitted for duplicate)
[INFO] |  |  |  |  |     +- org.jboss.marshalling:jboss-marshalling:jar:1.4.4.Final:compile
[INFO] |  |  |  |  |     \- (org.jboss.logging:jboss-logging:jar:3.1.2.GA:compile - omitted for conflict with 3.1.0.GA)
[INFO] |  |  |  |  \- (org.projectodd.wunderboss:wunderboss-clojure:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  +- org.immutant:scheduling:jar:2.0.2:compile
[INFO] |  |  |  |  +- (org.immutant:core:jar:2.0.2:compile - omitted for duplicate)
[INFO] |  |  |  |  +- org.projectodd.wunderboss:wunderboss-scheduling:jar:0.8.1:compile
[INFO] |  |  |  |  |  +- (org.projectodd.wunderboss:wunderboss-core:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  |  |  \- org.quartz-scheduler:quartz:jar:2.2.1:compile
[INFO] |  |  |  |  \- (org.projectodd.wunderboss:wunderboss-clojure:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  +- org.immutant:web:jar:2.0.2:compile
[INFO] |  |  |  |  +- (org.immutant:core:jar:2.0.2:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (potemkin:potemkin:jar:0.3.12:compile - omitted for duplicate)
[INFO] |  |  |  |  +- org.projectodd.wunderboss:wunderboss-web:jar:0.8.1:compile
[INFO] |  |  |  |  |  +- (org.projectodd.wunderboss:wunderboss-core:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- io.undertow:undertow-core:jar:1.1.0.Final:compile
[INFO] |  |  |  |  |  |  +- (org.jboss.logging:jboss-logging:jar:3.1.4.GA:compile - omitted for conflict with 3.1.0.GA)
[INFO] |  |  |  |  |  |  +- org.jboss.xnio:xnio-api:jar:3.3.0.Final:compile
[INFO] |  |  |  |  |  |  \- org.jboss.xnio:xnio-nio:jar:3.3.0.Final:runtime
[INFO] |  |  |  |  |  |     \- (org.jboss.xnio:xnio-api:jar:3.3.0.Final:runtime - omitted for duplicate)
[INFO] |  |  |  |  |  +- io.undertow:undertow-servlet:jar:1.1.0.Final:compile
[INFO] |  |  |  |  |  |  +- (io.undertow:undertow-core:jar:1.1.0.Final:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- org.jboss.spec.javax.servlet:jboss-servlet-api_3.1_spec:jar:1.0.0.Final:compile
[INFO] |  |  |  |  |  |  \- org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:jar:1.0.0.Final:compile
[INFO] |  |  |  |  |  \- io.undertow:undertow-websockets-jsr:jar:1.1.0.Final:compile
[INFO] |  |  |  |  |     +- (io.undertow:undertow-core:jar:1.1.0.Final:compile - omitted for duplicate)
[INFO] |  |  |  |  |     +- (io.undertow:undertow-servlet:jar:1.1.0.Final:compile - omitted for duplicate)
[INFO] |  |  |  |  |     \- org.jboss.spec.javax.websocket:jboss-websocket-api_1.1_spec:jar:1.1.0.Final:compile
[INFO] |  |  |  |  +- (ring:ring-core:jar:1.3.1:compile - omitted for conflict with 1.2.0)
[INFO] |  |  |  |  \- (org.projectodd.wunderboss:wunderboss-clojure:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  \- org.projectodd.wunderboss:wunderboss-clojure:jar:0.8.1:compile
[INFO] |  |  |     \- org.projectodd.wunderboss:wunderboss-core:jar:0.8.1:compile
[INFO] |  |  |        +- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |        +- ch.qos.logback:logback-classic:jar:1.1.2:compile
[INFO] |  |  |        |  +- ch.qos.logback:logback-core:jar:1.1.2:compile
[INFO] |  |  |        |  \- (org.slf4j:slf4j-api:jar:1.7.6:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |        \- (org.jboss.logging:jboss-logging:jar:3.1.4.GA:compile - omitted for conflict with 3.1.0.GA)
[INFO] |  |  +- org.immutant:wildfly:jar:2.0.2:compile
[INFO] |  |  |  +- org.immutant:core:jar:2.0.2:compile
[INFO] |  |  |  |  +- org.clojure:java.classpath:jar:0.2.2:compile
[INFO] |  |  |  |  +- (org.clojure:tools.reader:jar:0.8.13:compile - omitted for conflict with 0.10.0-alpha3)
[INFO] |  |  |  |  \- (org.projectodd.wunderboss:wunderboss-clojure:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.clojure:tools.nrepl:jar:0.2.7:compile - omitted for conflict with 0.2.3)
[INFO] |  |  |  +- org.projectodd.wunderboss:wunderboss-wildfly:jar:0.8.1:compile
[INFO] |  |  |  \- (org.projectodd.wunderboss:wunderboss-clojure:jar:0.8.1:compile - omitted for duplicate)
[INFO] |  |  +- liberator:liberator:jar:0.13:compile
[INFO] |  |  |  +- (org.clojure:data.json:jar:0.2.1:compile - omitted for conflict with 0.2.6)
[INFO] |  |  |  \- (org.clojure:data.csv:jar:0.1.2:compile - omitted for conflict with 0.1.3)
[INFO] |  |  +- compojure:compojure:jar:1.4.0:compile
[INFO] |  |  |  +- (org.clojure:tools.macro:jar:0.1.5:compile - omitted for conflict with 0.1.5)
[INFO] |  |  |  +- clout:clout:jar:2.1.2:compile
[INFO] |  |  |  |  \- instaparse:instaparse:jar:1.4.0:compile
[INFO] |  |  |  +- medley:medley:jar:0.6.0:compile
[INFO] |  |  |  +- (ring:ring-core:jar:1.4.0:compile - omitted for conflict with 1.2.0)
[INFO] |  |  |  \- ring:ring-codec:jar:1.0.0:compile
[INFO] |  |  +- ring-cors:ring-cors:jar:0.1.4:compile
[INFO] |  |  +- ring:ring-core:jar:1.4.0:compile
[INFO] |  |  |  +- (ring:ring-codec:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  |  |  +- (commons-io:commons-io:jar:2.4:compile - omitted for conflict with 2.3)
[INFO] |  |  |  +- commons-fileupload:commons-fileupload:jar:1.3.1:compile
[INFO] |  |  |  |  \- (commons-io:commons-io:jar:2.2:compile - omitted for conflict with 2.3)
[INFO] |  |  |  +- crypto-random:crypto-random:jar:1.2.0:compile
[INFO] |  |  |  \- crypto-equality:crypto-equality:jar:1.0.0:compile
[INFO] |  |  +- ring:ring-devel:jar:1.4.0:compile
[INFO] |  |  |  +- (ring:ring-core:jar:1.4.0:compile - omitted for duplicate)
[INFO] |  |  |  +- (hiccup:hiccup:jar:1.0.5:compile - omitted for duplicate)
[INFO] |  |  |  +- clj-stacktrace:clj-stacktrace:jar:0.2.8:compile
[INFO] |  |  |  \- ns-tracker:ns-tracker:jar:0.3.0:compile
[INFO] |  |  |     +- (org.clojure:tools.namespace:jar:0.2.10:compile - omitted for duplicate)
[INFO] |  |  |     \- (org.clojure:java.classpath:jar:0.2.2:compile - omitted for duplicate)
[INFO] |  |  +- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  |  +- (prismatic:schema:jar:0.4.2:compile - omitted for conflict with 1.0.1)
[INFO] |  |  +- (clj-time:clj-time:jar:0.9.0:compile - omitted for conflict with 0.11.0)
[INFO] |  |  +- (com.cognitect:transit-clj:jar:0.8.275:compile - omitted for conflict with 0.8.269)
[INFO] |  |  +- (com.cognitect:transit-cljs:jar:0.8.220:compile - omitted for conflict with 0.8.205)
[INFO] |  |  +- (com.palletops:leaven:jar:0.3.1:compile - omitted for conflict with 0.3.0)
[INFO] |  |  +- (com.taoensso:sente:jar:1.5.0:compile - omitted for duplicate)
[INFO] |  |  +- (com.taoensso:timbre:jar:4.0.2:compile - omitted for conflict with 4.1.1)
[INFO] |  |  \- (com.cemerick:austin:jar:0.1.6:compile - omitted for duplicate)
[INFO] |  +- (cenx:pack:jar:0.1.5:compile - omitted for duplicate)
[INFO] |  +- cenx:telemachus:jar:0.0.8:compile
[INFO] |  |  +- (com.palletops:leaven:jar:0.3.1:compile - omitted for conflict with 0.3.0)
[INFO] |  |  +- (metrics-clojure:metrics-clojure:jar:2.5.1:compile - omitted for duplicate)
[INFO] |  |  +- com.codahale.metrics:metrics-graphite:jar:3.0.2:compile
[INFO] |  |  |  +- (com.codahale.metrics:metrics-core:jar:3.0.2:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (com.taoensso:timbre:jar:4.1.4:compile - omitted for conflict with 4.1.1)
[INFO] |  |  \- (cenx:stentor:jar:1.5.18:compile - omitted for duplicate)
[INFO] |  +- (cenx:hades:jar:0.3.4:compile - omitted for duplicate)
[INFO] |  +- (zookeeper-clj:zookeeper-clj:jar:0.9.4:compile - omitted for conflict with 0.9.3)
[INFO] |  +- (http-kit:http-kit:jar:2.1.19:compile - omitted for duplicate)
[INFO] |  \- potemkin:potemkin:jar:0.3.12:compile
[INFO] |     +- clj-tuple:clj-tuple:jar:0.1.7:compile
[INFO] |     \- riddley:riddley:jar:0.1.7:compile
[INFO] +- cenx:athena:jar:1.2.51:compile
[INFO] |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  +- camel-snake-kebab:camel-snake-kebab:jar:0.1.5:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (cenx:anamnesis:jar:1.8.3:compile - omitted for conflict with 1.4.3)
[INFO] |  +- cenx:factum:jar:1.7.8:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  |  +- (cenx:proteus:jar:1.3.2:compile - omitted for conflict with 0.3.0)
[INFO] |  |  +- (cenx:alexandria:jar:2.1.6:compile - omitted for conflict with 2.2.0)
[INFO] |  |  +- (prismatic:schema:jar:0.4.0:compile - omitted for conflict with 1.0.1)
[INFO] |  |  +- (inflections:inflections:jar:0.9.13:compile - omitted for conflict with 0.9.14)
[INFO] |  |  +- (im.chit:ribol:jar:0.4.0:compile - omitted for duplicate)
[INFO] |  |  +- (clj-time:clj-time:jar:0.9.0:compile - omitted for conflict with 0.11.0)
[INFO] |  |  +- (com.taoensso:timbre:jar:3.4.0:compile - omitted for conflict with 4.1.1)
[INFO] |  |  +- (com.datomic:datomic-pro:jar:0.9.5350:compile - omitted for conflict with 0.9.5153)
[INFO] |  |  +- org.postgresql:postgresql:jar:9.3-1102-jdbc41:compile
[INFO] |  |  \- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.9:compile - omitted for conflict with 2.1.6)
[INFO] |  +- (cenx:plutus:jar:0.1.6:compile - omitted for duplicate)
[INFO] |  +- (cenx:alexandria:jar:2.1.13:compile - omitted for conflict with 2.2.0)
[INFO] |  +- environ:environ:jar:1.0.0:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.2.1:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (cenx:corona:jar:2.7.7:compile - omitted for conflict with 2.4.3)
[INFO] |  +- (org.clojure:core.memoize:jar:0.5.6:compile - omitted for conflict with 0.5.8)
[INFO] |  +- cenx:janus:jar:0.21.0:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  |  \- (prismatic:schema:jar:0.4.4:compile - omitted for conflict with 1.0.1)
[INFO] |  \- (cenx:stentor:jar:1.5.18:compile - omitted for duplicate)
[INFO] +- cenx:hades:jar:0.3.4:compile
[INFO] |  +- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] |  +- (com.palletops:leaven:jar:0.3.0:compile - omitted for duplicate)
[INFO] |  +- (com.taoensso:timbre:jar:4.3.1:compile - omitted for conflict with 4.1.1)
[INFO] |  \- (zookeeper-clj:zookeeper-clj:jar:0.9.4:compile - omitted for conflict with 0.9.3)
[INFO] +- org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile
[INFO] |  \- org.clojure:tools.analyzer.jvm:jar:0.1.0-beta12:compile
[INFO] |     +- org.clojure:tools.analyzer:jar:0.1.0-beta12:compile
[INFO] |     |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |     +- (org.clojure:core.memoize:jar:0.5.6:compile - omitted for conflict with 0.5.8)
[INFO] |     +- org.ow2.asm:asm-all:jar:4.1:compile
[INFO] |     \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] +- org.clojure:tools.reader:jar:0.9.2:compile
[INFO] |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] +- com.datomic:datomic-pro:jar:0.9.5153:compile
[INFO] |  +- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- org.clojure:tools.cli:jar:0.2.2:compile
[INFO] |  +- org.fressian:fressian:jar:0.6.5:compile
[INFO] |  +- commons-codec:commons-codec:jar:1.5:compile
[INFO] |  +- org.slf4j:jul-to-slf4j:jar:1.7.7:compile
[INFO] |  |  \- (org.slf4j:slf4j-api:jar:1.7.7:compile - omitted for conflict with 1.7.12)
[INFO] |  +- org.slf4j:slf4j-nop:jar:1.7.7:compile
[INFO] |  |  \- (org.slf4j:slf4j-api:jar:1.7.7:compile - omitted for conflict with 1.7.12)
[INFO] |  +- org.slf4j:log4j-over-slf4j:jar:1.7.7:runtime
[INFO] |  |  \- (org.slf4j:slf4j-api:jar:1.7.7:runtime - omitted for conflict with 1.7.12)
[INFO] |  +- org.slf4j:jcl-over-slf4j:jar:1.7.7:compile
[INFO] |  |  \- (org.slf4j:slf4j-api:jar:1.7.7:compile - omitted for conflict with 1.7.12)
[INFO] |  +- org.hornetq:hornetq-server:jar:2.3.17.Final:compile
[INFO] |  |  +- org.jboss.logging:jboss-logging:jar:3.1.0.GA:compile
[INFO] |  |  +- org.hornetq:hornetq-commons:jar:2.3.17.Final:compile
[INFO] |  |  |  +- (org.jboss.logging:jboss-logging:jar:3.1.0.GA:compile - omitted for duplicate)
[INFO] |  |  |  \- (io.netty:netty:jar:3.6.7.Final:compile - omitted for duplicate)
[INFO] |  |  +- org.hornetq:hornetq-journal:jar:2.3.17.Final:compile
[INFO] |  |  |  +- (org.jboss.logging:jboss-logging:jar:3.1.0.GA:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.hornetq:hornetq-commons:jar:2.3.17.Final:compile - omitted for duplicate)
[INFO] |  |  +- org.hornetq:hornetq-core-client:jar:2.3.17.Final:compile
[INFO] |  |  |  +- org.jgroups:jgroups:jar:3.2.12.Final:compile
[INFO] |  |  |  +- (org.hornetq:hornetq-commons:jar:2.3.17.Final:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.hornetq:hornetq-journal:jar:2.3.17.Final:compile - omitted for duplicate)
[INFO] |  |  |  \- (io.netty:netty:jar:3.6.7.Final:compile - omitted for duplicate)
[INFO] |  |  \- io.netty:netty:jar:3.6.7.Final:compile
[INFO] |  +- com.h2database:h2:jar:1.3.171:compile
[INFO] |  +- com.datomic:datomic-lucene-core:jar:3.3.0:compile
[INFO] |  +- com.google.guava:guava:jar:18.0:compile
[INFO] |  +- net.spy:spymemcached:jar:2.11.4:compile
[INFO] |  +- com.amazonaws:aws-java-sdk:jar:1.8.11:compile
[INFO] |  |  \- com.amazonaws:aws-java-sdk-core:jar:1.8.11:compile
[INFO] |  +- org.apache.httpcomponents:httpclient:jar:4.2:compile
[INFO] |  |  +- org.apache.httpcomponents:httpcore:jar:4.2:compile
[INFO] |  |  \- (commons-codec:commons-codec:jar:1.6:compile - omitted for conflict with 1.5)
[INFO] |  +- org.apache.tomcat:tomcat-jdbc:jar:7.0.27:compile
[INFO] |  |  \- org.apache.tomcat:tomcat-juli:jar:7.0.27:compile
[INFO] |  \- org.codehaus.janino:commons-compiler-jdk:jar:2.6.1:compile
[INFO] |     \- org.codehaus.janino:commons-compiler:jar:2.6.1:compile
[INFO] +- com.taoensso:timbre:jar:4.1.1:compile
[INFO] |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  +- com.taoensso:encore:jar:2.4.2:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- (org.clojure:tools.reader:jar:0.9.2:compile - omitted for duplicate)
[INFO] |  \- io.aviso:pretty:jar:0.1.18:compile
[INFO] |     \- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] +- com.stuartsierra:component:jar:0.2.3:compile
[INFO] |  \- com.stuartsierra:dependency:jar:0.1.1:compile
[INFO] +- cc.qbits:alia:jar:2.5.2:compile
[INFO] |  +- (org.clojure:clojure:jar:1.7.0-RC2:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (org.clojure:core.memoize:jar:0.5.6:compile - omitted for conflict with 0.5.8)
[INFO] |  +- org.clojure:core.cache:jar:0.6.4:compile
[INFO] |  |  +- org.clojure:data.priority-map:jar:0.0.4:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- cc.qbits:hayt:jar:3.0.0-rc1:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.7.0-beta1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- cc.qbits:commons:jar:0.3.0:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- (org.apache.commons:commons-lang3:jar:3.4:compile - omitted for conflict with 3.3.2)
[INFO] |  +- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.6:compile - omitted for duplicate)
[INFO] |  +- com.datastax.cassandra:cassandra-driver-dse:jar:2.1.6:compile
[INFO] |  |  \- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.6:compile - omitted for duplicate)
[INFO] |  \- (org.clojure:core.async:jar:0.1.346.0-17112a-alpha:compile - omitted for duplicate)
[INFO] +- clj-time:clj-time:jar:0.11.0:compile
[INFO] |  +- joda-time:joda-time:jar:2.8.2:compile
[INFO] |  \- (org.clojure:clojure:jar:1.7.0:compile - omitted for duplicate)
[INFO] +- clj-kafka:clj-kafka:jar:0.2.1-0.8:compile
[INFO] |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (zookeeper-clj:zookeeper-clj:jar:0.9.3:compile - omitted for duplicate)
[INFO] |  +- org.clojure:data.json:jar:0.2.2:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.3.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- com.101tec:zkclient:jar:0.3:compile
[INFO] |  |  \- (log4j:log4j:jar:1.2.14:compile - omitted for conflict with 1.2.17)
[INFO] |  +- com.yammer.metrics:metrics-core:jar:2.2.0:compile
[INFO] |  |  \- (org.slf4j:slf4j-api:jar:1.7.2:compile - omitted for conflict with 1.7.12)
[INFO] |  \- net.sf.jopt-simple:jopt-simple:jar:3.2:compile
[INFO] +- com.datastax.cassandra:cassandra-driver-core:jar:2.1.6:compile
[INFO] |  +- io.netty:netty-handler:jar:4.0.27.Final:compile
[INFO] |  |  +- io.netty:netty-buffer:jar:4.0.27.Final:compile
[INFO] |  |  |  \- io.netty:netty-common:jar:4.0.27.Final:compile
[INFO] |  |  +- io.netty:netty-transport:jar:4.0.27.Final:compile
[INFO] |  |  |  \- (io.netty:netty-buffer:jar:4.0.27.Final:compile - omitted for duplicate)
[INFO] |  |  \- io.netty:netty-codec:jar:4.0.27.Final:compile
[INFO] |  |     \- (io.netty:netty-transport:jar:4.0.27.Final:compile - omitted for duplicate)
[INFO] |  +- (com.google.guava:guava:jar:14.0.1:compile - omitted for conflict with 18.0)
[INFO] |  \- com.codahale.metrics:metrics-core:jar:3.0.2:compile
[INFO] |     \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] +- com.datastax.spark:spark-cassandra-connector-java_2.10:jar:1.2.0:compile
[INFO] |  +- (org.scala-lang:scala-library:jar:2.10.5:compile - omitted for conflict with 2.10.4)
[INFO] |  +- (com.datastax.spark:spark-cassandra-connector_2.10:jar:1.2.0:compile - omitted for duplicate)
[INFO] |  +- (com.codahale.metrics:metrics-core:jar:3.0.2:compile - omitted for duplicate)
[INFO] |  +- org.apache.cassandra:cassandra-thrift:jar:2.1.3:compile
[INFO] |  |  +- (org.apache.commons:commons-lang3:jar:3.1:compile - omitted for conflict with 3.4)
[INFO] |  |  +- (org.slf4j:slf4j-api:jar:1.7.2:compile - omitted for conflict with 1.7.12)
[INFO] |  |  \- org.apache.thrift:libthrift:jar:0.9.2:compile
[INFO] |  |     +- (org.slf4j:slf4j-api:jar:1.5.8:compile - omitted for conflict with 1.7.12)
[INFO] |  |     +- (org.apache.httpcomponents:httpclient:jar:4.2.5:compile - omitted for conflict with 4.2)
[INFO] |  |     \- (org.apache.httpcomponents:httpcore:jar:4.2.4:compile - omitted for conflict with 4.2)
[INFO] |  +- org.apache.cassandra:cassandra-clientutil:jar:2.1.3:compile
[INFO] |  +- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.5:compile - omitted for conflict with 2.1.6)
[INFO] |  +- org.apache.commons:commons-lang3:jar:3.3.2:compile
[INFO] |  +- (com.google.guava:guava:jar:14.0.1:compile - omitted for conflict with 18.0)
[INFO] |  +- org.joda:joda-convert:jar:1.2:compile
[INFO] |  +- (joda-time:joda-time:jar:2.3:compile - omitted for conflict with 2.8.2)
[INFO] |  +- com.twitter:jsr166e:jar:1.1.0:compile
[INFO] |  \- org.scala-lang:scala-reflect:jar:2.10.5:compile
[INFO] |     \- (org.scala-lang:scala-library:jar:2.10.5:compile - omitted for conflict with 2.10.4)
[INFO] +- com.datastax.spark:spark-cassandra-connector_2.10:jar:1.2.0:compile
[INFO] |  +- (org.scala-lang:scala-library:jar:2.10.5:compile - omitted for conflict with 2.10.4)
[INFO] |  +- (com.codahale.metrics:metrics-core:jar:3.0.2:compile - omitted for duplicate)
[INFO] |  +- (org.apache.cassandra:cassandra-thrift:jar:2.1.3:compile - omitted for duplicate)
[INFO] |  +- (org.apache.cassandra:cassandra-clientutil:jar:2.1.3:compile - omitted for duplicate)
[INFO] |  +- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.5:compile - omitted for conflict with 2.1.6)
[INFO] |  +- (org.apache.commons:commons-lang3:jar:3.3.2:compile - omitted for duplicate)
[INFO] |  +- (com.google.guava:guava:jar:14.0.1:compile - omitted for conflict with 18.0)
[INFO] |  +- (org.joda:joda-convert:jar:1.2:compile - omitted for duplicate)
[INFO] |  +- (joda-time:joda-time:jar:2.3:compile - omitted for conflict with 2.8.2)
[INFO] |  +- (com.twitter:jsr166e:jar:1.1.0:compile - omitted for duplicate)
[INFO] |  \- (org.scala-lang:scala-reflect:jar:2.10.5:compile - omitted for duplicate)
[INFO] +- org.codehaus.jackson:jackson-core-asl:jar:1.9.13:compile
[INFO] +- org.codehaus.jackson:jackson-mapper-asl:jar:1.9.13:compile
[INFO] |  \- (org.codehaus.jackson:jackson-core-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] +- com.datastax.spark:spark-cassandra-connector-embedded_2.10:jar:1.2.0:compile
[INFO] |  +- (org.scala-lang:scala-library:jar:2.10.5:compile - omitted for conflict with 2.10.4)
[INFO] |  +- org.apache.spark:spark-core_2.10:jar:1.2.1:compile
[INFO] |  |  +- com.twitter:chill_2.10:jar:0.5.0:compile
[INFO] |  |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  |  +- (com.twitter:chill-java:jar:0.5.0:compile - omitted for duplicate)
[INFO] |  |  |  \- (com.esotericsoftware.kryo:kryo:jar:2.21:compile - omitted for duplicate)
[INFO] |  |  +- com.twitter:chill-java:jar:0.5.0:compile
[INFO] |  |  |  \- (com.esotericsoftware.kryo:kryo:jar:2.21:compile - omitted for duplicate)
[INFO] |  |  +- org.apache.hadoop:hadoop-client:jar:2.2.0:compile
[INFO] |  |  |  +- (org.apache.hadoop:hadoop-common:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.hadoop:hadoop-hdfs:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  +- org.apache.hadoop:hadoop-mapreduce-client-app:jar:2.2.0:compile
[INFO] |  |  |  |  +- org.apache.hadoop:hadoop-mapreduce-client-common:jar:2.2.0:compile
[INFO] |  |  |  |  |  +- (org.apache.hadoop:hadoop-yarn-common:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- org.apache.hadoop:hadoop-yarn-client:jar:2.2.0:compile
[INFO] |  |  |  |  |  |  +- (org.apache.hadoop:hadoop-yarn-api:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (org.apache.hadoop:hadoop-yarn-common:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (commons-io:commons-io:jar:2.1:compile - omitted for conflict with 2.3)
[INFO] |  |  |  |  |  |  +- (com.google.inject:guice:jar:3.0:compile - omitted for conflict with 2.0)
[INFO] |  |  |  |  |  |  +- (com.sun.jersey.jersey-test-framework:jersey-test-framework-grizzly2:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (com.sun.jersey:jersey-server:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (com.sun.jersey:jersey-json:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  \- (com.sun.jersey.contribs:jersey-guice:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (org.apache.hadoop:hadoop-mapreduce-client-core:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- org.apache.hadoop:hadoop-yarn-server-common:jar:2.2.0:compile
[INFO] |  |  |  |  |  |  +- (org.apache.hadoop:hadoop-yarn-common:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  |  |  |  +- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (commons-io:commons-io:jar:2.1:compile - omitted for conflict with 2.3)
[INFO] |  |  |  |  |  |  +- (com.google.inject:guice:jar:3.0:compile - omitted for conflict with 2.0)
[INFO] |  |  |  |  |  |  +- (com.sun.jersey.jersey-test-framework:jersey-test-framework-grizzly2:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (com.sun.jersey:jersey-server:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  +- (com.sun.jersey:jersey-json:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  |  \- (com.sun.jersey.contribs:jersey-guice:jar:1.9:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- org.apache.hadoop:hadoop-mapreduce-client-shuffle:jar:2.2.0:compile
[INFO] |  |  |  |  |  +- (org.apache.hadoop:hadoop-mapreduce-client-core:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- org.apache.hadoop:hadoop-yarn-api:jar:2.2.0:compile
[INFO] |  |  |  |  +- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (commons-io:commons-io:jar:2.1:compile - omitted for conflict with 2.3)
[INFO] |  |  |  +- org.apache.hadoop:hadoop-mapreduce-client-core:jar:2.2.0:compile
[INFO] |  |  |  |  +- org.apache.hadoop:hadoop-yarn-common:jar:2.2.0:compile
[INFO] |  |  |  |  |  +- (log4j:log4j:jar:1.2.17:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (org.apache.hadoop:hadoop-yarn-api:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  |  \- (commons-io:commons-io:jar:2.1:compile - omitted for conflict with 2.3)
[INFO] |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- org.apache.hadoop:hadoop-mapreduce-client-jobclient:jar:2.2.0:compile
[INFO] |  |  |  |  +- (org.apache.hadoop:hadoop-mapreduce-client-common:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (org.apache.hadoop:hadoop-mapreduce-client-shuffle:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (com.google.protobuf:protobuf-java:jar:2.5.0:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  \- (org.apache.hadoop:hadoop-annotations:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  +- org.apache.spark:spark-network-common_2.10:jar:1.2.1:compile
[INFO] |  |  |  +- (io.netty:netty-all:jar:4.0.23.Final:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  |  +- org.apache.spark:spark-network-shuffle_2.10:jar:1.2.1:compile
[INFO] |  |  |  +- (org.apache.spark:spark-network-common_2.10:jar:1.2.1:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  |  +- net.java.dev.jets3t:jets3t:jar:0.7.1:compile
[INFO] |  |  |  +- (commons-codec:commons-codec:jar:1.3:compile - omitted for conflict with 1.5)
[INFO] |  |  |  \- (commons-httpclient:commons-httpclient:jar:3.1:compile - omitted for duplicate)
[INFO] |  |  +- org.apache.curator:curator-recipes:jar:2.4.0:compile
[INFO] |  |  |  +- org.apache.curator:curator-framework:jar:2.4.0:compile
[INFO] |  |  |  |  +- org.apache.curator:curator-client:jar:2.4.0:compile
[INFO] |  |  |  |  |  +- (org.slf4j:slf4j-api:jar:1.6.4:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  |  \- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  |  \- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  \- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  +- org.eclipse.jetty:jetty-plus:jar:8.1.14.v20131031:compile
[INFO] |  |  |  +- org.eclipse.jetty.orbit:javax.transaction:jar:1.1.1.v201105210645:compile
[INFO] |  |  |  +- (org.eclipse.jetty:jetty-webapp:jar:8.1.14.v20131031:compile - omitted for conflict with 8.1.10.v20130312)
[INFO] |  |  |  \- org.eclipse.jetty:jetty-jndi:jar:8.1.14.v20131031:compile
[INFO] |  |  |     +- (org.eclipse.jetty:jetty-server:jar:8.1.14.v20131031:compile - omitted for conflict with 8.1.10.v20130312)
[INFO] |  |  |     \- org.eclipse.jetty.orbit:javax.mail.glassfish:jar:1.4.1.v201005082020:compile
[INFO] |  |  |        \- org.eclipse.jetty.orbit:javax.activation:jar:1.1.0.v201105071233:compile
[INFO] |  |  +- org.eclipse.jetty:jetty-security:jar:8.1.14.v20131031:compile
[INFO] |  |  |  \- (org.eclipse.jetty:jetty-server:jar:8.1.14.v20131031:compile - omitted for conflict with 8.1.10.v20130312)
[INFO] |  |  +- org.eclipse.jetty:jetty-util:jar:8.1.14.v20131031:compile
[INFO] |  |  +- org.eclipse.jetty:jetty-server:jar:8.1.14.v20131031:compile
[INFO] |  |  |  +- (org.eclipse.jetty.orbit:javax.servlet:jar:3.0.0.v201112011016:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.eclipse.jetty:jetty-continuation:jar:8.1.14.v20131031:compile - omitted for conflict with 8.1.10.v20130312)
[INFO] |  |  |  \- (org.eclipse.jetty:jetty-http:jar:8.1.14.v20131031:compile - omitted for conflict with 8.1.10.v20130312)
[INFO] |  |  +- (org.apache.commons:commons-lang3:jar:3.3.2:compile - omitted for duplicate)
[INFO] |  |  +- org.apache.commons:commons-math3:jar:3.1.1:compile
[INFO] |  |  +- com.google.code.findbugs:jsr305:jar:1.3.9:compile
[INFO] |  |  +- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (org.slf4j:jul-to-slf4j:jar:1.7.5:compile - omitted for conflict with 1.7.7)
[INFO] |  |  +- (org.slf4j:jcl-over-slf4j:jar:1.7.5:compile - omitted for conflict with 1.7.7)
[INFO] |  |  +- (log4j:log4j:jar:1.2.17:compile - omitted for duplicate)
[INFO] |  |  +- com.ning:compress-lzf:jar:1.0.0:compile
[INFO] |  |  +- org.xerial.snappy:snappy-java:jar:1.1.1.6:compile
[INFO] |  |  +- net.jpountz.lz4:lz4:jar:1.2.0:compile
[INFO] |  |  +- org.roaringbitmap:RoaringBitmap:jar:0.4.5:compile
[INFO] |  |  +- (commons-net:commons-net:jar:2.2:compile - omitted for conflict with 3.3)
[INFO] |  |  +- org.spark-project.akka:akka-remote_2.10:jar:2.3.4-spark:compile
[INFO] |  |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  |  +- org.spark-project.akka:akka-actor_2.10:jar:2.3.4-spark:compile
[INFO] |  |  |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  |  |  \- com.typesafe:config:jar:1.2.1:compile
[INFO] |  |  |  +- (io.netty:netty:jar:3.8.0.Final:compile - omitted for conflict with 3.6.7.Final)
[INFO] |  |  |  +- org.spark-project.protobuf:protobuf-java:jar:2.5.0-spark:compile
[INFO] |  |  |  \- org.uncommons.maths:uncommons-maths:jar:1.2.2a:compile
[INFO] |  |  +- org.spark-project.akka:akka-slf4j_2.10:jar:2.3.4-spark:compile
[INFO] |  |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.spark-project.akka:akka-actor_2.10:jar:2.3.4-spark:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  +- org.json4s:json4s-jackson_2.10:jar:3.2.10:compile
[INFO] |  |  |  +- (org.scala-lang:scala-library:jar:2.10.0:compile - omitted for conflict with 2.10.4)
[INFO] |  |  |  +- org.json4s:json4s-core_2.10:jar:3.2.10:compile
[INFO] |  |  |  |  +- (org.scala-lang:scala-library:jar:2.10.0:compile - omitted for conflict with 2.10.4)
[INFO] |  |  |  |  +- org.json4s:json4s-ast_2.10:jar:3.2.10:compile
[INFO] |  |  |  |  |  \- (org.scala-lang:scala-library:jar:2.10.0:compile - omitted for conflict with 2.10.4)
[INFO] |  |  |  |  +- (com.thoughtworks.paranamer:paranamer:jar:2.6:compile - omitted for conflict with 2.3)
[INFO] |  |  |  |  \- org.scala-lang:scalap:jar:2.10.0:compile
[INFO] |  |  |  |     \- (org.scala-lang:scala-compiler:jar:2.10.0:compile - omitted for conflict with 2.10.4)
[INFO] |  |  |  \- (com.fasterxml.jackson.core:jackson-databind:jar:2.3.1:compile - omitted for conflict with 2.3.0)
[INFO] |  |  +- org.apache.mesos:mesos:jar:shaded-protobuf:0.18.1:compile
[INFO] |  |  +- io.netty:netty-all:jar:4.0.23.Final:compile
[INFO] |  |  +- com.clearspring.analytics:stream:jar:2.7.0:compile
[INFO] |  |  +- (com.codahale.metrics:metrics-core:jar:3.0.0:compile - omitted for conflict with 3.0.2)
[INFO] |  |  +- com.codahale.metrics:metrics-jvm:jar:3.0.0:compile
[INFO] |  |  |  +- (com.codahale.metrics:metrics-core:jar:3.0.0:compile - omitted for conflict with 3.0.2)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- com.codahale.metrics:metrics-json:jar:3.0.0:compile
[INFO] |  |  |  +- (com.codahale.metrics:metrics-core:jar:3.0.0:compile - omitted for conflict with 3.0.2)
[INFO] |  |  |  +- (com.fasterxml.jackson.core:jackson-databind:jar:2.2.2:compile - omitted for conflict with 2.3.1)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (com.codahale.metrics:metrics-graphite:jar:3.0.0:compile - omitted for conflict with 3.0.2)
[INFO] |  |  +- org.tachyonproject:tachyon-client:jar:0.5.0:compile
[INFO] |  |  |  \- org.tachyonproject:tachyon:jar:0.5.0:compile
[INFO] |  |  |     +- (org.slf4j:slf4j-api:jar:1.7.2:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |     +- (log4j:log4j:jar:1.2.17:compile - omitted for duplicate)
[INFO] |  |  |     +- (commons-io:commons-io:jar:2.4:compile - omitted for conflict with 2.3)
[INFO] |  |  |     +- (org.apache.commons:commons-lang3:jar:3.0:compile - omitted for conflict with 3.3.2)
[INFO] |  |  |     \- (com.fasterxml.jackson.core:jackson-databind:jar:2.3.0:compile - omitted for conflict with 2.3.1)
[INFO] |  |  +- org.spark-project:pyrolite:jar:2.0.1:compile
[INFO] |  |  +- net.sf.py4j:py4j:jar:0.8.2.1:compile
[INFO] |  |  \- org.spark-project.spark:unused:jar:1.0.0:compile
[INFO] |  +- org.apache.spark:spark-streaming_2.10:jar:1.2.1:compile
[INFO] |  |  +- (org.apache.spark:spark-core_2.10:jar:1.2.1:compile - omitted for duplicate)
[INFO] |  |  +- (org.eclipse.jetty:jetty-server:jar:8.1.14.v20131031:compile - omitted for duplicate)
[INFO] |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  +- org.apache.spark:spark-sql_2.10:jar:1.2.1:compile
[INFO] |  |  +- (org.apache.spark:spark-catalyst_2.10:jar:1.2.1:compile - omitted for duplicate)
[INFO] |  |  +- com.twitter:parquet-column:jar:1.6.0rc3:compile
[INFO] |  |  |  +- com.twitter:parquet-common:jar:1.6.0rc3:compile
[INFO] |  |  |  +- com.twitter:parquet-encoding:jar:1.6.0rc3:compile
[INFO] |  |  |  |  +- (com.twitter:parquet-common:jar:1.6.0rc3:compile - omitted for duplicate)
[INFO] |  |  |  |  +- com.twitter:parquet-generator:jar:1.6.0rc3:compile
[INFO] |  |  |  |  |  \- (com.twitter:parquet-common:jar:1.6.0rc3:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (commons-codec:commons-codec:jar:1.5:compile - omitted for duplicate)
[INFO] |  |  |  \- (commons-codec:commons-codec:jar:1.5:compile - omitted for duplicate)
[INFO] |  |  +- com.twitter:parquet-hadoop:jar:1.6.0rc3:compile
[INFO] |  |  |  +- (com.twitter:parquet-column:jar:1.6.0rc3:compile - omitted for duplicate)
[INFO] |  |  |  +- com.twitter:parquet-format:jar:2.2.0-rc1:compile
[INFO] |  |  |  +- com.twitter:parquet-jackson:jar:1.6.0rc3:compile
[INFO] |  |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.11:compile - omitted for conflict with 1.9.13)
[INFO] |  |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.11:compile - omitted for conflict with 1.9.13)
[INFO] |  |  |  \- (org.xerial.snappy:snappy-java:jar:1.0.5:compile - omitted for conflict with 1.1.1.6)
[INFO] |  |  +- com.fasterxml.jackson.core:jackson-databind:jar:2.3.0:compile
[INFO] |  |  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.3.0:compile
[INFO] |  |  |  \- (com.fasterxml.jackson.core:jackson-core:jar:2.3.0:compile - omitted for conflict with 2.4.4)
[INFO] |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  +- org.apache.spark:spark-catalyst_2.10:jar:1.2.1:compile
[INFO] |  |  +- org.scala-lang:scala-compiler:jar:2.10.4:compile
[INFO] |  |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.scala-lang:scala-reflect:jar:2.10.4:compile - omitted for conflict with 2.10.5)
[INFO] |  |  +- (org.scala-lang:scala-reflect:jar:2.10.4:compile - omitted for conflict with 2.10.5)
[INFO] |  |  +- org.scalamacros:quasiquotes_2.10:jar:2.0.1:compile
[INFO] |  |  |  +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.scala-lang:scala-reflect:jar:2.10.4:compile - omitted for conflict with 2.10.5)
[INFO] |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  +- org.apache.spark:spark-hive_2.10:jar:1.2.1:compile
[INFO] |  |  +- (org.apache.spark:spark-sql_2.10:jar:1.2.1:compile - omitted for duplicate)
[INFO] |  |  +- org.spark-project.hive:hive-metastore:jar:0.13.1a:compile
[INFO] |  |  |  +- (org.spark-project.hive:hive-serde:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  +- org.spark-project.hive:hive-shims:jar:0.13.1a:compile
[INFO] |  |  |  |  +- org.spark-project.hive.shims:hive-shims-common:jar:0.13.1a:compile
[INFO] |  |  |  |  |  +- (commons-logging:commons-logging:jar:1.1.3:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (log4j:log4j:jar:1.2.16:compile - omitted for conflict with 1.2.17)
[INFO] |  |  |  |  |  +- (org.apache.thrift:libthrift:jar:0.9.0:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- org.spark-project.hive.shims:hive-shims-0.20:jar:0.13.1a:runtime
[INFO] |  |  |  |  |  +- (org.spark-project.hive.shims:hive-shims-common:jar:0.13.1a:runtime - omitted for duplicate)
[INFO] |  |  |  |  |  +- (org.mortbay.jetty:jetty:jar:6.1.26:compile - scope updated from runtime; omitted for duplicate)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:runtime - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- org.spark-project.hive.shims:hive-shims-common-secure:jar:0.13.1a:compile
[INFO] |  |  |  |  |  +- (org.spark-project.hive.shims:hive-shims-common:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (commons-codec:commons-codec:jar:1.4:compile - omitted for conflict with 1.5)
[INFO] |  |  |  |  |  +- (commons-lang:commons-lang:jar:2.4:compile - omitted for conflict with 2.6)
[INFO] |  |  |  |  |  +- (commons-logging:commons-logging:jar:1.1.3:compile - omitted for duplicate)
[INFO] |  |  |  |  |  +- (org.apache.thrift:libthrift:jar:0.9.0:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  |  |  +- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- org.spark-project.hive.shims:hive-shims-0.20S:jar:0.13.1a:runtime
[INFO] |  |  |  |  |  +- (org.spark-project.hive.shims:hive-shims-common-secure:jar:0.13.1a:runtime - omitted for duplicate)
[INFO] |  |  |  |  |  +- (org.mortbay.jetty:jetty:jar:6.1.26:runtime - omitted for duplicate)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:runtime - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  +- org.spark-project.hive.shims:hive-shims-0.23:jar:0.13.1a:runtime
[INFO] |  |  |  |  |  +- (org.spark-project.hive.shims:hive-shims-common-secure:jar:0.13.1a:runtime - omitted for duplicate)
[INFO] |  |  |  |  |  +- (commons-lang:commons-lang:jar:2.4:runtime - omitted for conflict with 2.6)
[INFO] |  |  |  |  |  +- (commons-logging:commons-logging:jar:1.1.3:runtime - omitted for duplicate)
[INFO] |  |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:runtime - omitted for conflict with 1.7.12)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- com.jolbox:bonecp:jar:0.8.0.RELEASE:compile
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.2:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- (commons-cli:commons-cli:jar:1.2:compile - omitted for duplicate)
[INFO] |  |  |  +- (commons-lang:commons-lang:jar:2.4:compile - omitted for conflict with 2.6)
[INFO] |  |  |  +- (commons-logging:commons-logging:jar:1.1.3:compile - omitted for duplicate)
[INFO] |  |  |  +- org.apache.derby:derby:jar:10.10.1.1:compile
[INFO] |  |  |  +- org.datanucleus:datanucleus-api-jdo:jar:3.2.6:compile
[INFO] |  |  |  +- org.datanucleus:datanucleus-core:jar:3.2.10:compile
[INFO] |  |  |  +- org.datanucleus:datanucleus-rdbms:jar:3.2.9:compile
[INFO] |  |  |  +- javax.jdo:jdo-api:jar:3.0.1:compile
[INFO] |  |  |  |  \- javax.transaction:jta:jar:1.1:compile
[INFO] |  |  |  +- (org.antlr:antlr-runtime:jar:3.4:compile - omitted for conflict with 3.5)
[INFO] |  |  |  +- org.apache.thrift:libfb303:jar:0.9.0:compile
[INFO] |  |  |  |  \- (org.apache.thrift:libthrift:jar:0.9.0:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  +- (org.apache.thrift:libthrift:jar:0.9.0:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- commons-httpclient:commons-httpclient:jar:3.1:compile
[INFO] |  |  |  +- (commons-logging:commons-logging:jar:1.0.4:compile - omitted for conflict with 1.1.3)
[INFO] |  |  |  \- (commons-codec:commons-codec:jar:1.2:compile - omitted for conflict with 1.5)
[INFO] |  |  +- org.spark-project.hive:hive-exec:jar:0.13.1a:compile
[INFO] |  |  |  +- org.spark-project.hive:hive-ant:jar:0.13.1a:compile
[INFO] |  |  |  |  +- (commons-lang:commons-lang:jar:2.4:compile - omitted for conflict with 2.6)
[INFO] |  |  |  |  +- (org.apache.ant:ant:jar:1.9.1:compile - omitted for duplicate)
[INFO] |  |  |  |  +- org.apache.velocity:velocity:jar:1.5:compile
[INFO] |  |  |  |  |  +- commons-collections:commons-collections:jar:3.1:compile
[INFO] |  |  |  |  |  +- (commons-lang:commons-lang:jar:2.1:compile - omitted for conflict with 2.6)
[INFO] |  |  |  |  |  \- oro:oro:jar:2.0.8:compile
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- org.spark-project.hive:hive-common:jar:0.13.1a:compile
[INFO] |  |  |  |  +- (org.spark-project.hive:hive-shims:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (commons-cli:commons-cli:jar:1.2:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (commons-lang:commons-lang:jar:2.4:compile - omitted for conflict with 2.6)
[INFO] |  |  |  |  +- (log4j:log4j:jar:1.2.16:compile - omitted for conflict with 1.2.17)
[INFO] |  |  |  |  +- (org.apache.commons:commons-compress:jar:1.4.1:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- (org.spark-project.hive:hive-metastore:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.spark-project.hive:hive-serde:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.spark-project.hive:hive-shims:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  +- com.twitter:parquet-hadoop-bundle:jar:1.3.2:compile
[INFO] |  |  |  +- (commons-codec:commons-codec:jar:1.4:compile - omitted for conflict with 1.5)
[INFO] |  |  |  +- (commons-httpclient:commons-httpclient:jar:3.0.1:compile - omitted for conflict with 3.1)
[INFO] |  |  |  +- (commons-io:commons-io:jar:2.4:compile - omitted for conflict with 2.3)
[INFO] |  |  |  +- (org.apache.commons:commons-lang3:jar:3.1:compile - omitted for conflict with 3.3.2)
[INFO] |  |  |  +- (commons-lang:commons-lang:jar:2.4:compile - omitted for conflict with 2.6)
[INFO] |  |  |  +- javolution:javolution:jar:5.5.1:compile
[INFO] |  |  |  +- (log4j:log4j:jar:1.2.16:compile - omitted for conflict with 1.2.17)
[INFO] |  |  |  +- (org.antlr:antlr-runtime:jar:3.4:compile - omitted for conflict with 3.5)
[INFO] |  |  |  +- org.antlr:ST4:jar:4.0.4:compile
[INFO] |  |  |  |  \- (org.antlr:antlr-runtime:jar:3.3:compile - omitted for conflict with 3.5)
[INFO] |  |  |  +- (org.apache.avro:avro:jar:1.7.5:compile - omitted for duplicate)
[INFO] |  |  |  +- org.apache.avro:avro-mapred:jar:1.7.5:compile
[INFO] |  |  |  |  +- (org.apache.avro:avro-ipc:jar:1.7.5:compile - omitted for conflict with 1.7.6)
[INFO] |  |  |  |  +- (org.apache.avro:avro-ipc:jar:tests:1.7.5:compile - omitted for conflict with 1.7.6)
[INFO] |  |  |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.6.4:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- org.apache.ant:ant:jar:1.9.1:compile
[INFO] |  |  |  |  \- org.apache.ant:ant-launcher:jar:1.9.1:compile
[INFO] |  |  |  +- (org.apache.commons:commons-compress:jar:1.4.1:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.thrift:libfb303:jar:0.9.0:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.thrift:libthrift:jar:0.9.0:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  +- (org.apache.zookeeper:zookeeper:jar:3.4.5:compile - omitted for conflict with 3.4.0)
[INFO] |  |  |  +- org.codehaus.groovy:groovy-all:jar:2.1.6:compile
[INFO] |  |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.2:compile - omitted for conflict with 1.9.13)
[INFO] |  |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.2:compile - omitted for conflict with 1.9.13)
[INFO] |  |  |  +- (org.datanucleus:datanucleus-core:jar:3.2.10:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.spark-project.protobuf:protobuf-java:jar:2.5.0-spark:compile - omitted for duplicate)
[INFO] |  |  |  +- com.googlecode.javaewah:JavaEWAH:jar:0.3.2:compile
[INFO] |  |  |  +- org.iq80.snappy:snappy:jar:0.2:compile
[INFO] |  |  |  +- org.json:json:jar:20090211:compile
[INFO] |  |  |  +- stax:stax-api:jar:1.0.1:compile
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.8.8:compile - omitted for conflict with 1.9.13)
[INFO] |  |  +- org.spark-project.hive:hive-serde:jar:0.13.1a:compile
[INFO] |  |  |  +- (org.spark-project.hive:hive-common:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.spark-project.hive:hive-shims:jar:0.13.1a:compile - omitted for duplicate)
[INFO] |  |  |  +- (commons-codec:commons-codec:jar:1.4:compile - omitted for conflict with 1.5)
[INFO] |  |  |  +- (commons-lang:commons-lang:jar:2.4:compile - omitted for conflict with 2.6)
[INFO] |  |  |  +- (org.apache.avro:avro:jar:1.7.5:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.apache.thrift:libthrift:jar:0.9.0:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (org.apache.avro:avro:jar:1.7.6:compile - omitted for conflict with 1.7.5)
[INFO] |  |  +- org.apache.avro:avro-mapred:jar:hadoop2:1.7.6:compile
[INFO] |  |  |  +- org.apache.avro:avro-ipc:jar:1.7.6:compile
[INFO] |  |  |  |  +- (org.apache.avro:avro:jar:1.7.6:compile - omitted for conflict with 1.7.5)
[INFO] |  |  |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.6.4:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- org.apache.avro:avro-ipc:jar:tests:1.7.6:compile
[INFO] |  |  |  |  +- (org.apache.avro:avro:jar:1.7.6:compile - omitted for conflict with 1.7.5)
[INFO] |  |  |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  |  \- (org.slf4j:slf4j-api:jar:1.6.4:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.13:compile - omitted for duplicate)
[INFO] |  |  |  \- (org.slf4j:slf4j-api:jar:1.6.4:compile - omitted for conflict with 1.7.12)
[INFO] |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  +- (org.apache.cassandra:cassandra-thrift:jar:2.1.3:compile - omitted for duplicate)
[INFO] |  +- (org.apache.cassandra:cassandra-clientutil:jar:2.1.3:compile - omitted for duplicate)
[INFO] |  +- (com.datastax.cassandra:cassandra-driver-core:jar:2.1.5:compile - omitted for conflict with 2.1.6)
[INFO] |  +- org.apache.cassandra:cassandra-all:jar:2.1.3:compile
[INFO] |  |  +- (org.xerial.snappy:snappy-java:jar:1.0.5:compile - omitted for conflict with 1.1.1.6)
[INFO] |  |  +- (net.jpountz.lz4:lz4:jar:1.2.0:compile - omitted for duplicate)
[INFO] |  |  +- (com.ning:compress-lzf:jar:0.8.4:compile - omitted for conflict with 1.0.0)
[INFO] |  |  +- (com.google.guava:guava:jar:16.0:compile - omitted for conflict with 18.0)
[INFO] |  |  +- commons-cli:commons-cli:jar:1.1:compile
[INFO] |  |  +- (commons-codec:commons-codec:jar:1.2:compile - omitted for conflict with 1.5)
[INFO] |  |  +- (org.apache.commons:commons-lang3:jar:3.1:compile - omitted for conflict with 3.3.2)
[INFO] |  |  +- (org.apache.commons:commons-math3:jar:3.2:compile - omitted for conflict with 3.1.1)
[INFO] |  |  +- com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru:jar:1.3:compile
[INFO] |  |  +- org.antlr:antlr:jar:3.5.2:compile
[INFO] |  |  |  +- (org.antlr:antlr-runtime:jar:3.5.2:compile - omitted for conflict with 3.5)
[INFO] |  |  |  \- (org.antlr:ST4:jar:4.0.8:compile - omitted for conflict with 4.0.4)
[INFO] |  |  +- org.antlr:antlr-runtime:jar:3.5.2:compile
[INFO] |  |  +- org.antlr:stringtemplate:jar:4.0.2:compile
[INFO] |  |  |  +- (org.antlr:stringtemplate:jar:3.2.1:compile - omitted for cycle)
[INFO] |  |  |  \- (org.antlr:antlr-runtime:jar:3.3:compile - omitted for conflict with 3.5.2)
[INFO] |  |  +- (org.slf4j:slf4j-api:jar:1.7.2:compile - omitted for conflict with 1.7.12)
[INFO] |  |  +- (org.codehaus.jackson:jackson-core-asl:jar:1.9.2:compile - omitted for conflict with 1.9.13)
[INFO] |  |  +- (org.codehaus.jackson:jackson-mapper-asl:jar:1.9.2:compile - omitted for conflict with 1.9.13)
[INFO] |  |  +- jline:jline:jar:1.0:compile
[INFO] |  |  +- com.googlecode.json-simple:json-simple:jar:1.1:compile
[INFO] |  |  +- com.boundary:high-scale-lib:jar:1.0.6:compile
[INFO] |  |  +- org.yaml:snakeyaml:jar:1.11:compile
[INFO] |  |  +- org.mindrot:jbcrypt:jar:0.3m:compile
[INFO] |  |  +- (com.yammer.metrics:metrics-core:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  +- com.addthis.metrics:reporter-config:jar:2.1.0:compile
[INFO] |  |  |  +- (org.slf4j:slf4j-api:jar:1.7.2:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  +- (org.yaml:snakeyaml:jar:1.12:compile - omitted for conflict with 1.11)
[INFO] |  |  |  +- org.hibernate:hibernate-validator:jar:4.3.0.Final:compile
[INFO] |  |  |  |  +- javax.validation:validation-api:jar:1.0.0.GA:compile
[INFO] |  |  |  |  \- (org.jboss.logging:jboss-logging:jar:3.1.0.CR2:compile - omitted for conflict with 3.1.0.GA)
[INFO] |  |  |  \- (com.yammer.metrics:metrics-core:jar:2.2.0:compile - omitted for duplicate)
[INFO] |  |  +- com.thinkaurelius.thrift:thrift-server:jar:0.3.7:compile
[INFO] |  |  |  +- com.lmax:disruptor:jar:3.0.1:compile
[INFO] |  |  |  +- (org.apache.thrift:libthrift:jar:0.9.1:compile - omitted for conflict with 0.9.2)
[INFO] |  |  |  +- (org.slf4j:slf4j-api:jar:1.6.1:compile - omitted for conflict with 1.7.12)
[INFO] |  |  |  \- junit:junit:jar:4.8.1:compile
[INFO] |  |  +- (com.clearspring.analytics:stream:jar:2.5.2:compile - omitted for conflict with 2.7.0)
[INFO] |  |  +- net.sf.supercsv:super-csv:jar:2.1.0:compile
[INFO] |  |  +- (org.apache.thrift:libthrift:jar:0.9.2:compile - omitted for duplicate)
[INFO] |  |  +- (org.apache.cassandra:cassandra-thrift:jar:2.1.3:compile - omitted for duplicate)
[INFO] |  |  +- net.java.dev.jna:jna:jar:4.0.0:compile
[INFO] |  |  +- com.github.jbellis:jamm:jar:0.3.0:compile
[INFO] |  |  \- (io.netty:netty-all:jar:4.0.23.Final:compile - omitted for duplicate)
[INFO] |  +- (net.sf.jopt-simple:jopt-simple:jar:3.2:compile - omitted for duplicate)
[INFO] |  +- org.apache.spark:spark-repl_2.10:jar:1.2.1:compile
[INFO] |  |  +- org.scala-lang:jline:jar:2.10.4:compile
[INFO] |  |  |  \- org.fusesource.jansi:jansi:jar:1.4:compile
[INFO] |  |  +- (org.apache.spark:spark-core_2.10:jar:1.2.1:compile - omitted for duplicate)
[INFO] |  |  +- (org.eclipse.jetty:jetty-server:jar:8.1.14.v20131031:compile - omitted for duplicate)
[INFO] |  |  +- (org.scala-lang:scala-reflect:jar:2.10.4:compile - omitted for conflict with 2.10.5)
[INFO] |  |  +- (org.slf4j:jul-to-slf4j:jar:1.7.5:compile - omitted for conflict with 1.7.7)
[INFO] |  |  \- (org.spark-project.spark:unused:jar:1.0.0:compile - omitted for duplicate)
[INFO] |  \- org.apache.kafka:kafka_2.10:jar:0.8.2.1:compile
[INFO] |     +- (com.yammer.metrics:metrics-core:jar:2.2.0:compile - omitted for duplicate)
[INFO] |     +- (org.scala-lang:scala-library:jar:2.10.4:compile - omitted for duplicate)
[INFO] |     +- (org.apache.kafka:kafka-clients:jar:0.8.2.1:compile - omitted for duplicate)
[INFO] |     +- (org.apache.zookeeper:zookeeper:jar:3.4.6:compile - omitted for conflict with 3.4.0)
[INFO] |     \- (com.101tec:zkclient:jar:0.3:compile - omitted for duplicate)
[INFO] +- org.scala-lang:scala-library:jar:2.10.5:compile
[INFO] +- t6:from-scala:jar:0.3.0:compile
[INFO] |  +- funcool:cats:jar:1.2.1:compile
[INFO] |  \- (potemkin:potemkin:jar:0.4.3:compile - omitted for conflict with 0.3.12)
[INFO] +- gorillalabs:sparkling:jar:1.2.3:compile
[INFO] |  +- org.clojure:tools.logging:jar:0.3.1:compile
[INFO] |  |  \- (org.clojure:clojure:jar:1.4.0:compile - omitted for conflict with 1.7.0)
[INFO] |  +- (clj-time:clj-time:jar:0.9.0:compile - omitted for conflict with 0.11.0)
[INFO] |  +- com.twitter:carbonite:jar:1.4.0:compile
[INFO] |  |  +- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- com.esotericsoftware.kryo:kryo:jar:2.21:compile
[INFO] |  |     +- com.esotericsoftware.reflectasm:reflectasm:jar:shaded:1.07:compile
[INFO] |  |     |  \- (org.ow2.asm:asm:jar:4.0:compile - omitted for conflict with 4.1)
[INFO] |  |     +- com.esotericsoftware.minlog:minlog:jar:1.2:compile
[INFO] |  |     \- org.objenesis:objenesis:jar:1.2:compile
[INFO] |  +- com.damballa:parkour:jar:0.6.2:compile
[INFO] |  |  +- (org.clojure:tools.logging:jar:0.3.1:compile - omitted for duplicate)
[INFO] |  |  +- (com.damballa:abracad:jar:0.4.11:compile - omitted for duplicate)
[INFO] |  |  +- (org.apache.avro:avro:jar:1.7.7:compile - omitted for conflict with 1.7.5)
[INFO] |  |  +- org.platypope:letterpress:jar:0.1.0:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- pjstadig:scopes:jar:0.3.0:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  +- transduce:transduce:jar:0.1.1:compile
[INFO] |  |  |  \- (org.clojure:clojure:jar:1.5.1:compile - omitted for conflict with 1.7.0)
[INFO] |  |  \- (org.clojure:clojure:jar:1.6.0:compile - omitted for conflict with 1.7.0)
[INFO] |  \- (com.damballa:abracad:jar:0.4.12:compile - omitted for conflict with 0.4.11)
[INFO] +- org.clojure:data.csv:jar:0.1.3:test (scope not updated to compile)
[INFO] |  \- (org.clojure:clojure:jar:1.4.0:test - omitted for conflict with 1.7.0)
[INFO] +- org.clojure:tools.namespace:jar:0.2.10:test (scope not updated to compile)
[INFO] |  \- (org.clojure:clojure:jar:1.4.0:test - omitted for conflict with 1.7.0)
[INFO] +- org.clojure:test.check:jar:0.9.0:test (scope not updated to compile)
[INFO] |  \- (org.clojure:clojure:jar:1.7.0:test - omitted for duplicate)
[INFO] +- cenx:experior:jar:0.4.0:test (scope not updated to compile)
[INFO] |  +- (com.taoensso:timbre:jar:3.4.0:test - omitted for conflict with 4.1.1)
[INFO] |  \- (com.datomic:datomic-pro:jar:0.9.5153:test - omitted for duplicate)
[INFO] \- midje:midje:jar:1.7.0:test
[INFO]    +- (org.clojure:clojure:jar:1.6.0:test - omitted for conflict with 1.7.0)
[INFO]    +- ordered:ordered:jar:1.2.0:test
[INFO]    +- org.clojure:math.combinatorics:jar:0.1.1:test
[INFO]    |  \- (org.clojure:clojure:jar:1.4.0:test - omitted for conflict with 1.7.0)
[INFO]    +- org.clojure:core.unify:jar:0.5.2:test
[INFO]    +- (clj-time:clj-time:jar:0.9.0:test - omitted for conflict with 0.11.0)
[INFO]    +- colorize:colorize:jar:0.1.1:test
[INFO]    +- (org.clojure:tools.macro:jar:0.1.5:compile - scope updated from test; omitted for duplicate)
[INFO]    +- dynapath:dynapath:jar:0.2.0:test
[INFO]    +- org.tcrawley:dynapath:jar:0.2.3:test
[INFO]    +- swiss-arrows:swiss-arrows:jar:1.0.0:test
[INFO]    +- (org.clojure:tools.namespace:jar:0.2.7:test - omitted for conflict with 0.2.10)
[INFO]    +- flare:flare:jar:0.2.8:test
[INFO]    |  \- org.clojars.brenton:google-diff-match-patch:jar:0.1:test
[INFO]    +- (slingshot:slingshot:jar:0.12.1:compile - scope updated from test; omitted for duplicate)
[INFO]    \- (commons-codec:commons-codec:jar:1.10:test - omitted for conflict with 1.5)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 5.050 s
[INFO] Finished at: 2017-04-19T11:45:21-04:00
[INFO] Final Memory: 51M/1217M
[INFO] ------------------------------------------------------------------------
