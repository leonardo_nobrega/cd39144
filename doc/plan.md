
Implementation Plan

Questions to answer:

* the aggregator should run in spark;
how do I write to cassandra from a spark job?
    * https://github.com/datastax/spark-cassandra-connector/blob/master/doc/5_saving.md
    * cenx.prometheus.spark.cassandra/rdd->table

* how do I start a spark job from clojure?
    * need to create a spark context
    * spark context is in spark manager
    * prod.init/SparkManager (levski)

* how to test insertion in a cassandra table?
    * robot?
    * embedded cassandra in spark-cassandra-connector?

* should the long-term table have a default hard-coded ttl?

Tasks:

Cassandra initialization component:

* update the short-term table schema
    * add a creation-time column, that the aggregator will use to get "new data"
        * the timestamp in the message is not enough; a message having a timestamp
          that is older than the timestamp the aggregator expects will not go into
          the long-term table
    * new data may be:
      (1) data recently generated or
      (2) new version of data from the past
    * creation-time column is a clustering column;
      it changes too often to be in partition-key - think of chunk, which is week
* function that creates the long-term table
* function that creates a column in the long-term table
* location of the component:
  cenx.plataea.cassandra.init/CassandraSchema
  instantiated in prod.init/analytics-system (levski)

Short-term table:

* config param for ttl
* change the insert in plataea to use the ttl;
  location: cenx.plataea.cassandra.vnf/insert-metric-value

Aggregator:

 1. aggregator component in prod.init
 2. query that gets the time of the last row written to long-term
 3. query that gets the latest rows from short-term
 4. function that converts list of values to blob
 5. config param for long-table ttl
 6. function that converts blob to list of values
 7. function that populates long-term - uses 2, 3, 4, 5 and 6
 8. function for fixing / updating the long-term data (may be the same as 7)
 9. config param for the aggregation frequency
10. update delorean query
    location: cenx.delorean.resources.time-series/get-data
11. test for blob <-> value-list conversion
12. upgrade spark-cassandra connector in prometheus
13. test with embedded cassandra (depends on 11)
