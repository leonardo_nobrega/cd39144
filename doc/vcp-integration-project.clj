(defproject cenx/plataea "1.8.28.14-SNAPSHOT"
  :description "A clojure library for performing analytics calculations."
  :license {:name "CENX Commercial License"
            :url "http://www.cenx.com"}
  :java-opts ["-Xmx8g" "-Xms2g" "-server"]
  :aot [#"cenx\.plataea\.spark\..*"
        #"cenx\.plataea\.hybrid\..*"]
  :global-vars {*warn-on-reflection* true}
  :dependencies [[org.clojure/clojure "1.7.0"]

                 [cenx/prometheus "0.7.12"]
                 [cenx/ramesseum "2.4.63.15"]

                 [cenx/proteus "0.3.0" :exclusions [org.clojure/clojure]]
                 [cenx/stentor "1.5.18" :exclusions [org.clojure/clojure
                                                     org.jboss.logging/jboss-logging-spi
                                                     stylefruits/gniazdo
                                                     org.jboss.netty/netty]]
                 [cenx/plutus "0.1.6"]
                 [cenx/alexandria "2.2.0"]
                 [cenx/anamnesis "1.4.3" :exclusions [org.immutant/immutant
                                                      commons-fileupload/commons-fileupload]]
                 [cenx/flight "1.4.6" :exclusions [com.keminglabs/cljx]]
                 [cenx/pack "0.1.5" :exclusions [org.clojure/clojure]]
                 [cenx/baldr "1.3.1" :exclusions [org.clojure/clojure]]
                 [cenx/athena "1.2.51"]
                 [cenx/hades "0.3.4"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/tools.reader "0.9.2"]

                 [com.datomic/datomic-pro "0.9.5153" :exclusions [[joda-time]]]
                 [com.taoensso/timbre "4.1.1"]
                 [com.stuartsierra/component "0.2.3"]

                 [cc.qbits/alia "2.5.2" :exclusions [org.slf4j/log4j-over-slf4j
                                                     org.clojure/tools.reader]]
                 [clj-time "0.11.0"]
                 [clj-kafka "0.2.1-0.8" :exclusions [org.scala-lang/scala-library
                                                     org.apache.zookeeper/zookeeper
                                                     org.xerial.snappy/snappy-java]]]
  :exclusions [org.slf4j/slf4j-log4j12]
  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[org.clojure/data.csv "0.1.3"]
                                  [org.clojure/tools.namespace "0.2.10"]
                                  [org.clojure/test.check "0.9.0"]
                                  [cenx/experior "0.4.0" :exclusions [org.clojure/clojure]]
                                  [midje "1.7.0"]]
                   :plugins [[lein-midje "3.1.3"]
                             [lein-package "2.1.1"]
                             [lein-bikeshed "0.2.0"]
                             [jonase/eastwood "0.2.1"]
                             [lein-kibit "0.0.8"]]
                   :hooks [leiningen.package.hooks.deploy leiningen.package.hooks.install]
                   :package {:skipjar false
                             :autobuild true
                             :reuse false
                             :artifacts [{:build "uberjar" :classifier "standalone" :extension "jar"}]}}}
  :aliases {"lint" ["do" ["bikeshed"] ["eastwood"] ["kibit"]]
            "ci" ["do" ["test"] ["lint"] ["package"]]}
  :uberjar-exclusions [#"(?i)^META-INF/[^/]*\.SF$"] ;; Eliminate the signing code from the uberjar
  :repositories [["nexus" {:url "http://nexus.cenx.localnet:8081/nexus/content/groups/public"}]])
